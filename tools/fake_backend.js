const express = require('express')
const app = express()
var count = 0;

app.get('/', function (req, res) {
    res.send('Hello World!');
})

app.get('/api/wifilist', function (req, res) {
    console.log('Received wifilist GET request');
    json_data1 = '[{ "id": 0, "ssid": "SFR_81B0", "rssi": 100, "secured": true }, { "id": 1, "ssid": "Freebox_AE45", "rssi": 42, "secured": true }]';
    json_data2 = '[{ "id": 0, "ssid": "Livebox-212A", "rssi": 100, "secured": true }, { "id": 1, "ssid": "Freebox_AE45", "rssi": 42, "secured": true }]';
    res.setHeader('Content-Type', 'application/json');
    if(count % 2 == 0)
    {
        res.send(json_data1);
    }
    else
    {
        res.send(json_data2);
    }
    count += 1;
})

app.get('/api/reset', function (req, res) {
    console.log('Received RESET GET request');
    res.sendStatus(200).end();
})

app.get('/api/coordinatessave', function (req, res) {
    console.log('Received Save Coordinates GET request');
    console.log('latitude: ' + req.query.lat);
    console.log('longitude: ' + req.query.lon);
    res.sendStatus(200).end();
})

app.get('/api/wifisave', function (req, res) {
    console.log('Received Save Credentials GET request');
    console.log('ssid: ' + req.query.ssid);
    console.log('password: ' + req.query.password);
    res.sendStatus(200).end();
})

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
})
