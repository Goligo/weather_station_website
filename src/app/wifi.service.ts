import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { ScannedNetwork } from './scannednetwork';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})

export class WifiService {
  private wifiListUrl = 'api/wifilist';
  private wifiSaveUrl = 'api/wifisave';
  private coordinatesSaveUrl = 'api/coordinatessave';
  private resetUrl = 'api/reset';

  constructor(private http: HttpClient, private messageService: MessageService) { }

  /** GET: get scanned network list from server */
  getNetworks(): Observable<ScannedNetwork[]> {
    return this.http.get<ScannedNetwork[]>(this.wifiListUrl)
      .pipe(
        tap(networks => this.log('fetched networks')),
        catchError(this.handleError('getNetworks', []))
      );
  }

  /** GET: send a reset request to the board */
  sendReset(): Observable<any> {
    console.log("send reset");
    return this.http.get(this.resetUrl)
      .pipe(
        tap(networks => this.log('sent reset request')),
        catchError(this.handleError('sendReset', []))
      );
  }

  /** GET: send GPS coordinates to the server */
  saveCoordinates(latitude: number, longitude: number): Observable<any> {
    const parameters = new HttpParams()
      .set("lat", latitude.toString())
      .set("lon", longitude.toString());

    console.log("Send GET request to save GPS coordinates");

    return this.http.get(this.coordinatesSaveUrl, { params: parameters }).pipe(
      tap(_ => this.log(`saved GPS coordinates`)),
      catchError(this.handleError<any>('saveCoordinates'))
    );
  }

  /** GET: save WiFi credentials on the server */
  saveWifiCredentials(network: ScannedNetwork, password: string): Observable<any> {
    console.log("Send GET request to save wifi credentials: ssid: " + network.ssid + " password: " + password);

    const parameters = new HttpParams()
      .set("ssid", network.ssid)
      .set("password", password);

    return this.http.get(this.wifiSaveUrl, { params: parameters }).pipe(
      tap(_ => this.log(`saved Wifi credentials for network ${network.ssid}`)),
      catchError(this.handleError<any>('saveWifiCredentials'))
    );
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`WifiService: ${message}`);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
