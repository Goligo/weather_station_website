import { Component, OnInit } from '@angular/core';

import { WifiService } from '../wifi.service';

@Component({
  selector: 'app-localisator',
  templateUrl: './localisator.component.html',
  styleUrls: ['./localisator.component.css']
})
export class LocalisatorComponent implements OnInit {
  latitude: number;
  longitude: number;

  constructor(private wifiService: WifiService) { }

  ngOnInit() {
  }

  getCoordinates(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
      });
    } else {
      console.log("Localisation not supported by this browser");
    }
  }

  saveCoordinates(): void {
    this.wifiService.saveCoordinates(this.latitude,this.longitude).subscribe();
  }
}
