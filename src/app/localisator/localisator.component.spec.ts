import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalisatorComponent } from './localisator.component';

describe('LocalisatorComponent', () => {
  let component: LocalisatorComponent;
  let fixture: ComponentFixture<LocalisatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalisatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalisatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
