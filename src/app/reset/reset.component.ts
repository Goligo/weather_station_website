import { Component, OnInit } from '@angular/core';

import { WifiService } from '../wifi.service'

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  constructor(private wifiService: WifiService) { }

  ngOnInit() {
  }

  reset() {
    this.wifiService.sendReset().subscribe();
  }
}
