import { Component, OnInit } from '@angular/core';

import { ScannedNetwork } from '../scannednetwork';
import { WifiService } from '../wifi.service'

@Component({
  selector: 'app-wifi-list',
  templateUrl: './wifi-list.component.html',
  styleUrls: ['./wifi-list.component.css'],
})
export class WifiListComponent implements OnInit {
  scannedNetworks: ScannedNetwork[];

  selectedNetwork: ScannedNetwork;

  password: string;

  constructor(private wifiService: WifiService) { }

  ngOnInit() {
    this.getNetworks();
  }

  onSelect(network: ScannedNetwork): void {
    this.selectedNetwork = network;
  }

  getNetworks(): void {
    this.wifiService.getNetworks()
      .subscribe(scannedNetworks => this.scannedNetworks = scannedNetworks);
  }

  save(): void {
    console.log("save called");
    this.wifiService.saveWifiCredentials(this.selectedNetwork, this.password)
      .subscribe();
  }
}
