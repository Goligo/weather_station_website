import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const wifilist = [
      { id: 0, ssid: 'SFR_81B0', rssi: 100, secured: true },
      { id: 1, ssid: 'Freebox_AE45', rssi: 42, secured: true },
      { id: 2, ssid: 'Livebox_8134', rssi: 53, secured: true },
      { id: 3, ssid: 'touchepasamonreseausalevoison', rssi: 100, secured: true },
      { id: 4, ssid: 'coucouhadopi', rssi: 96, secured: true },
      { id: 5, ssid: 'FreeParty', rssi: 20, secured: false },
    ];
    return {wifilist};
  }
}