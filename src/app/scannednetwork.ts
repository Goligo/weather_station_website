export class ScannedNetwork {
    id: number;
    ssid: string;
    rssi: number;
    secured: boolean;    
}